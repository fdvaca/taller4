#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include "cifrado.h"

void clear_line(){
    int c;
    while((c=getchar())!='\n' && c!=EOF);
}

int main(){
    char abecedario[]={"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char mensaje[30];
    int t_cif, c_num;
    char c_letra[30];
    char cifrado[30]={0};
    printf("Ingrese un mensaje: ");
    fgets(mensaje, LINE_MAX, stdin);
    reemplazar(mensaje,'\n','\0');
    do{
        printf("Tipo de cifrado: (1) ciclico y (2) autollave: ");
        scanf("%d", &t_cif);
        clear_line();
    }while((t_cif!=1) && (t_cif!=2));
   
    if(t_cif==1){
        printf("Ingrese contraseña numerica: ");
        scanf("%d", &c_num);
        clear_line();
        cifrado_ciclico(cifrado,mensaje, c_num, abecedario);
    }else{
        printf("Ingrese contraseña alfabetica: ");
        fgets(c_letra, LINE_MAX, stdin);
        cifrado_autollave(cifrado,mensaje,c_letra,abecedario);
    }
    printf("El mensaje cifrado es: %s\n", cifrado);   
}
