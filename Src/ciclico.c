#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "cifrado.h"

int buscarIndice(char letra, char cadena[]){
    int i;
    letra=toupper(letra);
    for(i=0 ; i<strlen(cadena) ; i++){
        if(cadena[i]==letra){
            return i;
        }
    }
    return -1;
}

void cifrado_ciclico(char cifrado[], char mensaje[], int contra, char abecedario[]){

    int tam=strlen(mensaje);
    int i;

    if(contra>=0){
        for(i=0; i<tam; i++){
            int n=buscarIndice(mensaje[i], abecedario);
            int check=n+contra;
            if(check>strlen(abecedario)){
                cifrado[i]=abecedario[contra];
            }
            cifrado[i]=abecedario[check];
        }
    }else{
        for(i=0; i<tam; i++){
            int n=buscarIndice(mensaje[i], abecedario);
            int check=n+contra;
            if(check>=0){
                cifrado[i]=abecedario[check];
            }else{
                int m=strlen(abecedario)+contra;
                cifrado[i]=abecedario[m];
            }
        }
    }
}
