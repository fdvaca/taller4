Bin/main: Obj/main.o Obj/ciclico.o Obj/autollave.o
	gcc -Wall -o Bin/main Obj/main.o Obj/ciclico.o Obj/autollave.o

Obj/main.o: Src/main.c 
	gcc -Wall -c -o Obj/main.o Src/main.c -I./Include

Obj/ciclico.o: Src/ciclico.c Include/cifrado.h
	gcc -Wall -c -o Obj/ciclico.o Src/ciclico.c -I./Include

Obj/autollave.o: Src/autollave.c Include/cifrado.h
	gcc -Wall -c -o Obj/autollave.o Src/autollave.c -I./Include
